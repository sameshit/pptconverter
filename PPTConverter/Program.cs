﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ZMQ;
using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Core;
using System.IO;

namespace PPTConverter
{
    class Program
    {
        static byte[] ToBe32(int value)
        {
            byte[] bt = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(bt);

            return bt;
        }

        static void Main(string[] args)
        {
            ZMQ.Context ctx = new ZMQ.Context();
            ZMQ.Socket rep = ctx.Socket(SocketType.REP);
            Application pptApplication = new Application();

            rep.Bind("tcp://*:5555");

            while (true)
            {
                Boolean is_pptx;
                byte[] ppt_bytes = rep.Recv();
                string filename;
                byte[] export_bytes;
                MemoryStream ms = new MemoryStream();

                is_pptx = ppt_bytes[0] == 1;
                if (is_pptx)
                    filename = "temp.pptx";
                else
                    filename = "temp.ppt";
                    
                File.WriteAllBytes(filename,ppt_bytes.Skip(1).ToArray());

                string result_path = Directory.GetCurrentDirectory() +"\\" +filename;
                Presentation ppt = pptApplication.Presentations.Open(result_path, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);

                
                foreach (Slide slide in ppt.Slides)
                {
                    string export_path = Directory.GetCurrentDirectory() + "\\test.jpg";
                    slide.Export(export_path, "jpg");
                    export_bytes = File.ReadAllBytes(export_path);
                    ms.Write(ToBe32(export_bytes.Length), 0, 4);
                    ms.Write(export_bytes,0,export_bytes.Length);
                }

                rep.Send(ms.ToArray());
            }
        }
    }
}
